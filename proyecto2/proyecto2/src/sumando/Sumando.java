package sumando;

import java.util.Scanner;

public class Sumando {

	public static void main(String[] args) {
		int n1, n2;
		int suma;
		Scanner in = new Scanner(System.in);
		
		System.out.println("Introduce un numero: ");
		n1=in.nextInt();
		
		System.out.println("Introduce otro numero: ");
		n2=in.nextInt();
		
		suma=n1+n2;
		System.out.println("la suma de " + n1 + " y " + n2 +" es = " + suma);

	}

}
